//SERVER THAT HOLDS USER DATA USERNAME ASSOCIATED WITH NAME AND LASTNAME

//loading modules
//filesystem
var fs=require('fs');
//http
var http=require('http');
//web socket
var io=require('socket.io');
//couchdb interface
var couchdb=require('felix-couchdb');
//childporcess
var cp=require('child_process');
// uses childprocesses to create database if not yet created
// childprocess is used in case it blocks the server for a longer while
var dbcreate=cp.fork('dbcreate.js');


// loading needed files
var sockfile=fs.readFileSync('namespace.html');

//db data
var dbHost="127.0.0.1";
var dbPort=5984;
var dbName="users";

//server request handler
var server=http.createServer();
server.on('request', function(req, res){
	//sends needed files back
	res.writeHead(200, {'content-type':'text/html'});
	res.end(sockfile);
	//console.log("N");
});

// connectes web socket listner to server
var socket=io.listen(server);

//socket handler
socket.on('connection', function(connection){
	//if connection to clients is up
	console.log("connected uparunning");
	//sends back for client
	connection.emit('news',{contents:"serverconnection created"});
	//listens for user update on his data
	connection.on('userdataput', function (data){
		//creasts doc with the data passed through
		createDoc(data);
	});
	connection.on('userupdate', function(data){
		updateData(data);
	});

	//listens for data request
	connection.on('userdataget', function (data){
		//gets data with the id passed in the data obj, also passes the connection out of the listener( later idea for that to pass it to a child procss)
		getDoc(data, connection);
	});
	
	
});

//functions to create data doc
function createDoc(inf){
	//greates client for the data
	var client=couchdb.createClient(dbPort,dbHost);
	var db= client.db(dbName);

	// creates the object for the data object
	var user={
		names:{
			first: inf.first,
			last: inf.last
		}
	};

	// saves doc in database with fallback if err occuers
	db.saveDoc(inf.username,user,function(err, doc){
		if(err){
			console.log(JSON.stringify(err));
		}else{
			console.log('saved user');
			console.log(doc);
		}
	});
}

//gets data 
function getDoc(inf, connection){
	//creates data
	var client=couchdb.createClient(dbPort,dbHost);
	var db= client.db(dbName);
	
	//gets data with fallback in case of an err of doc is not defined
	db.getDoc(inf.username, function (err, doc){
		if(err){
			console.log(JSON.stringify(err));
		}else{

			if(doc){
				console.log(doc);
				connection.emit('ask',{contents: doc});
				connection.emit('news',{contents: doc});
			}else{
				console.log("not defined");

				connection.emit('news', {contents :"not defined"});
			}
		}
	
	});	
}
function updateData(inf){
	//updates the user data
	//first needs to get the old doc to get the rev
	var client=couchdb.createClient(dbPort,dbHost);
	var db= client.db(dbName);
	var rev;
	
	db.getDoc(inf.username, function (err, doc){
		if(err){
			console.log(JSON.stringify(err));
		}else{

			if(doc){
				rev = doc._rev;
				console.log(rev);
				var user={
					_rev:rev,
					names:{
						first: inf.first,
						last: inf.last
					}
				};
				db.saveDoc(inf.username,user,function(err, doc){
					if(err){
						console.log(JSON.stringify(err));
						console.log(doc);
					}else{
						console.log('saved user');
						console.log(doc);
					}
				});
			}else{
				connection.emit('noUpdate', {contents :"no doc to update"});

			}
		}
	
	});
	



}








server.listen(8080);
